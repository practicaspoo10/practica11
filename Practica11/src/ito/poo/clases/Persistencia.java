package ito.poo.clases;

import java.util.ArrayList;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Persistencia {
	
	public static void grabaVehiculos(ArrayList<Vehiculo> lista) {
		ObjectOutputStream file = null;
		try {
			file = new ObjectOutputStream(new FileOutputStream("datos.dat"));
			for (Vehiculo v : lista)
				file.writeObject(v);
			file.close();
			} catch (Exception e) {
				System.err.println(e.getStackTrace());
				}
		}
	public static ArrayList<Vehiculo> recuperaVehiculo() {
		ArrayList<Vehiculo> rv=new ArrayList<Vehiculo>();
		ObjectInputStream file=null;
		Vehiculo V=null;
		try {
			file = new ObjectInputStream(new FileInputStream("datos.dat"));
			while((V=(Vehiculo)file.readObject())!=null) {
				rv.add(V);
				}
			}
		catch (Exception e) {
			}
			return rv;

}
}
